function tinhThue(thuNhap){
    var duoi60 = 0.05;
    var tu60den120 = 0.1;
    var tu120den216= 0.15;
    var tu216den384 =0.2;
    if(thuNhap <60e+6){
        return thuNhap*duoi60;
    }else if (thuNhap < 120e+6){
        return 60e+6*duoi60+(thuNhap-60e+6)*tu60den120;
    }else if(thuNhap<216e+6){
        return 60e+6*duoi60+60e+6*tu60den120+(216e+6-thuNhap)*tu120den216;
    }else if(thuNhap<384e+6){
        return 60e+6*duoi60+60e+6*tu60den120+96e+6*tu120den216+ (384e+6-thuNhap)*tu216den384;
    }
}
function soNguoiPhuThuoc(tinhThue, soNguoi){
    return 0.015*tinhThue*soNguoi;
}
function tinhTien(){
    var hoTen=document.querySelector("#ho-ten").value;
    var thuNhap=document.querySelector("#tong-thu-nhap").value*1;
    var soNguoi=document.querySelector("#so-nguoi-phu-thuoc").value*1;
    //Tax
    var tienThue=tinhThue(thuNhap);
    var tienThuePhuThuDuaTrenSoNguoi=soNguoiPhuThuoc(tienThue,soNguoi);
    //Sum Tax
    var tongThue=tienThue+tienThuePhuThuDuaTrenSoNguoi;
    var tongThue_styled=new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'VND' }).format(tongThue)
    document.querySelector("#result").innerHTML=`Họ tên: ${hoTen}; Tiền thuế thu nhập cá nhân: ${tongThue_styled} `;
}
