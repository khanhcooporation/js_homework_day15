function dropLoaiKhachHang(loaiKhachHang){
    if(loaiKhachHang == "no-drop"){
        return 0;
    }else if(loaiKhachHang=="drop"){
        return 1;
    }else{
        return -1;
    }
}
function drop(){
    var loaiKhachHang=document.querySelector("#loai-khach-hang").value;
    var checkKhachHang=dropLoaiKhachHang(loaiKhachHang);
    if(checkKhachHang == 0 || checkKhachHang == -1){
        document.querySelector("#invisible-row").style.setProperty('display', 'none', 'important');
        return 0;
    }else if(checkKhachHang==1){
        document.querySelector("#invisible-row").style.setProperty('display', 'block', 'important');        
        return 1;
    }
}
//Tien Drop=========================================================================
function tienDrop(drop_value, soKetNoi){
    var soKetNoiUnder10=90;
    var soKetNoiFrom11=5;
    if(drop_value==1){
        if(soKetNoi<=10){
            return soKetNoiUnder10;
        }else{
            return soKetNoiUnder10+soKetNoiFrom11*(soKetNoi-10);
        }
    }else{
        return 0;
    }
}
function tienKetNoi_Drop(soKenh){
    return soKenh*50;
}
//=========================================================================
function alert_no_choice(){
    var loaiKhachHang=document.querySelector("#loai-khach-hang").value;
    var checkKhachHang=dropLoaiKhachHang(loaiKhachHang);
    if(checkKhachHang== -1){
        return 1;
    }
}

function tienKetNoi(soKenh){
    var giaTienGoc=25;
    var giaTienMoiKetNoi=7.5;
    var tongTien=giaTienGoc+soKenh*giaTienMoiKetNoi;
    return tongTien;
}
function tinhTien(){
    //Take the value
    var maKhachHang=document.querySelector("#ma-khach-hang").value;
    var soKenh=document.querySelector("#so-kenh").value*1;
    var soKetNoi=document.querySelector("#so-ket-noi").value*1;
    //alert khong chon loai khach hang
    var alertNoChoice=alert_no_choice();
    if (alertNoChoice==1){
        alert("Hãy chọn loại khách hàng");
        return 0;
    }
    // Tiền Drop
    var drop_value= drop();
    var tienKenh;
    var dropMoney;
    if(drop_value==1){
        dropMoney= tienDrop(drop_value,soKetNoi);
        tienKenh=tienKetNoi_Drop(soKenh);
    }else{
        //tien ket noi Ko Drop
        tienKenh=tienKetNoi(soKenh);
        dropMoney=0;
    }
    var tongTienCap=tienKenh+dropMoney;
    tongTienCap_style=new Intl.NumberFormat('en-IN', { style: 'currency', currency: 'EUR' }).format(tongTienCap);
    document.querySelector("#result").innerHTML=`Mã khách hàng: ${maKhachHang} ;Tiền Cáp: ${tongTienCap_style} `;


}