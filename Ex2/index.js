function tienkw(soKw){
    var tienDien1To50=500;
    var tienDien50to100=650;
    var tienDien100to200=850;
    var tienDien200to350=1100;
    var tienDien350Plus=1300;
    if(soKw <= 50){
        return soKw*tienDien1To50;
    }else if(50<soKw && soKw <= 100){
        return 50*tienDien1To50+(soKw-50)*tienDien50to100;
    }else if (100< soKw && soKw <= 200){
        return 50*tienDien1To50+50*tienDien50to100+(soKw-100)*tienDien100to200;
    }else if(200< soKw && soKw <=350){
        return 50*tienDien1To50+50*tienDien50to100+100*tienDien100to200+(soKw-200)*tienDien200to350;
    }else{
        return 50*tienDien1To50+50*tienDien50to100+100*tienDien100to200+150*tienDien200to350+ (soKw-350)*tienDien350Plus; 
    }
}

function alertTienDien(soKw){
    if(soKw==0){
        return 1;
    }else{
        return 0;
    }
}
function tinhTien(){
    var hoTen=document.querySelector("#ho-ten").value;
    var soKw=document.querySelector("#so-kw").value;
    if(alertTienDien(soKw)==1){
        alert("Số Kw không hợp lệ! Vui lòng nhập lại.");
        return 0;
    }
    var soTienDien=tienkw(soKw);
    document.querySelector("#result").innerHTML=`Họ tên: ${hoTen}; Tiền điện: ${soTienDien} `;
}